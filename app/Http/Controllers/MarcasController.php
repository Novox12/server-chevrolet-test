<?php

namespace App\Http\Controllers;

use App\Models\MarcaModel;
use Exception;
use Illuminate\Http\Request;

class MarcasController extends Controller
{
    public function marcas()
    {
        $marcas = MarcaModel::all();
        return response()->json(['ok'=>true,'marcas'=>$marcas],200);
    }

    public function marcasnew(Request $request)
    {
        $validateData = $request->validate([
            'marca'=>'required|string|max:255'
        ]);
    
        MarcaModel::create([
            'marca'=>$validateData['marca']
        ]);
        
        return response()->json(['ok'=>true,'msg'=>'La marca fue registra correctamente'],201);
    }
    
    public function  marcasupdate(Request $request , $id)
    {
        try{
            $marca = MarcaModel::find($id);
            $marca->marca = $request->input('marca');
            $marca->save();
    
            return response()->json(['ok'=>true,'msg'=>'La Marca fue actualizada correctamente'],200);
        }catch(Exception $e){
            return response()->json(['ok'=>false,'msg'=>'Error en los campos', 'err'=>$e],500);
        }
    }

}
