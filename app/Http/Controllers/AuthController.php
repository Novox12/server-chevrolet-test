<?php

namespace App\Http\Controllers;

use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function register (Request $request){
       
        $validateData = $request->validate([
            'name'=>'required|string|max:255',
            'email'=>'required|string|email|max:255|unique:users',
            'password'=>'required|string|min:8'
        ]);
    
        $user = User::create([
            'name'=>$validateData['name'],
            'email'=>$validateData['email'],
            'password'=> Hash::make($validateData['password'])
        ]);

        $token = $user->createToken('auth_token')->plainTextToken;

        return response()->json([
            'ok'=>true,
            'token_type' => 'Bearer',
            'access_token'=>$token
        ], 201);

       

    }

    public function login (Request $request){
        if(!Auth::attempt($request->only('email', 'password'))){
            return response()->json([
                'ok'=>false,
                'msg' => 'Usuario invalido'
            ], 401);
        }

        $user = User::where('email', $request['email'])->firstOrFail();

        $token = $user->createToken('auth_token')->plainTextToken;

        return response()->json([
            'ok'=>true,
            'token_type' => 'Bearer',
            'access_token'=>$token
        ], 200);
    }

    public function infouser (Request $request){
        return $request->user();
    }

}
