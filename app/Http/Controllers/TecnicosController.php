<?php

namespace App\Http\Controllers;

use App\Models\OrdenModel;
use App\Models\User;
use Illuminate\Http\Request;

class TecnicosController extends Controller
{
    public function tecnicosarea($id_sede, $id_area)
    {
        $tecnicos = User::select('*')->where([
            ['sede_id', '=', $id_sede],
            ['area_id', '=', $id_area]
        ])->where('role_id',4)->get();

        return response()->json(['ok'=>true,'tecnicos'=>$tecnicos],200);
    }

    public function tecnicosallsede($id)
    {
        $tecnicos = User::select('*')->where('sede_id', $id)->get();

        return response()->json(['ok'=>true,'tecnicos'=>$tecnicos],200);
    }

    public function tecnicoagenda($id)
    {
        $user_rol= User::find($id);
        if($user_rol->role_id == 4){
            $orden = OrdenModel::select('*')->where('user_id', $id)->get();
            return response()->json(['ok'=>true,'agenda'=>$orden],200);
        }else{
            return response()->json(['ok'=>false,'msg'=>'Este usuario no es tecnico'],401);
        }
    }
}
