<?php

use App\Http\Controllers\AreasController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\MarcasController;
use App\Http\Controllers\ModeloController;
use App\Http\Controllers\OrdenController;
use App\Http\Controllers\RolesController;
use App\Http\Controllers\SedesController;
use App\Http\Controllers\TecnicosController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\VehiculosController;
use App\Models\MarcaModel;
use App\Models\SedeModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login', [AuthController::class, 'login'] );
Route::post('/user/sede/new', [UserController::class, 'registeruser']);

Route::group(['middleware' => ['auth:sanctum']], function() {
    
    Route::get('/infouser', [AuthController::class, 'infouser']);
    Route::get('/user/all', [UserController::class, 'userall']);
    Route::get('/user/{id}', [UserController::class, 'userone']);
    Route::put('/user/update/{id}', [UserController::class, 'userupdate']);

    Route::get('/sedes', [SedesController::class, 'sedes']);
    Route::post('/sedes/new', [SedesController::class, 'sedesnew']);
    Route::put('/sedes/update/{id}', [SedesController::class, 'sedesupdate']);

    Route::get('/marcas', [MarcasController::class, 'marcas']);
    Route::post('/marcas/new', [MarcasController::class, 'marcasnew']);
    Route::put('/marcas/update/{id}', [MarcasController::class, 'marcasupdate']);

    Route::get('/vehiculos/all', [VehiculosController::class, 'vehiculosall']);
    Route::get('/vehiculos/{id}', [VehiculosController::class, 'vehiculos']);
    Route::post('/vehiculo/new', [VehiculosController::class, 'vehiculonew']);
    Route::put('/vehiculo/update/{id}', [VehiculosController::class, 'vehiculoupdate']);

    Route::get('/modelo/all', [ModeloController::class, 'modelo']);
    Route::post('/modelo/new', [ModeloController::class, 'modelonew']);
    Route::put('/modelo/update/{id}', [ModeloController::class, 'modeloupdate']);

    Route::get('/areas/all', [AreasController::class, 'areas']);
    Route::post('/areas/new', [AreasController::class, 'areasnew']);
    Route::put('/areas/update/{id}', [AreasController::class, 'areasupdate']);

    Route::get('/tecnicos/area/{id_sede}/{id_area}', [TecnicosController::class, 'tecnicosarea']);
    Route::get('/tecnicos/sede/{id}', [TecnicosController::class, 'tecnicosallsede']);
    Route::get('/tecnico/agenda/{id}', [TecnicosController::class, 'tecnicoagenda']);

    Route::post('/orden/new', [OrdenController::class, 'ordennew']);
    Route::get('/orden/sede/{id}', [OrdenController::class, 'ordensede']);

    Route::get('/roles', [RolesController::class, 'roles']);
});



