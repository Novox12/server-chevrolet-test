<?php

namespace App\Http\Controllers;

use App\Models\SedeModel;
use Exception;
use Illuminate\Http\Request;

class SedesController extends Controller
{
    public function sedes (){
        $roles = SedeModel::all();
        return response()->json(['ok'=>true,'sedes'=>$roles],200);
    }

    public function sedesnew(Request $request) {

        $validateData = $request->validate([
            'nombre'=>'required|string|max:255',
            'direccion'=>'required|string|max:255',
            'centro_costo'=>'required|string|max:255',
        ]);
    
        SedeModel::create([
            'nombre'=>$validateData['nombre'],
            'direccion'=>$validateData['direccion'],
            'centro_costo'=>$validateData['centro_costo']
        ]);
        
        return response()->json(['ok'=>true,'msg'=>'La sede se registro correctamente'],201);
        
    }

      
    public function  sedesupdate(Request $request , $id)
    {
        try{
            $sede = SedeModel::find($id);
            $sede->nombre = $request->input('nombre');
            $sede->direccion = $request->input('direccion');
            $sede->centro_costo = $request->input('centro_costo');
            $sede->save();
    
            return response()->json(['ok'=>true,'msg'=>'La Sede fue actualizada correctamente'],200);
        }catch(Exception $e){
            return response()->json(['ok'=>false,'msg'=>'Error en los campos', 'err'=>$e],500);
        }
    }

}
