<?php

namespace App\Http\Controllers;

use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;


class UserController extends Controller
{
    public function userall () {
        $users = User::all();
        return response()->json(['ok'=>true,'users'=>$users],200);
    }

    public function userone($id)
    {
        $user = User::find($id);
        return response()->json(['ok'=>true,'user'=>$user],200);
    }

    public function registeruser (Request $request)
    {
      
        $validateData = $request->validate([
            'name'=>'required|string|max:255',
            'apellido'=>'required|string|max:255',
            'cedula'=>'required|string|max:255',
            'email'=>'required|string|email|max:255|unique:users',
            'password'=>'required|string|min:8',
            'role_id'=>'required|integer',
            'sede_id'=>'required|integer',
            'area_id'=>'required|integer'
        ]);

        User::create([
            'name'=>$validateData['name'],
            'apellido'=>$validateData['apellido'],
            'cedula'=>$validateData['cedula'],
            'email'=>$validateData['email'],
            'password'=>Hash::make($validateData['password']),
            'role_id'=>$validateData['role_id'],
            'sede_id'=>$validateData['sede_id'],
            'area_id'=>$validateData['area_id']
        ]);


        return response()->json(['ok'=>true,'user'=>'Usuario creado'],200);
    }

    public function userupdate(Request $request, $id)
    {
        try{
            $user = User::find($id);
            $user->name = $request->input('name');
            $user->apellido = $request->input('apellido');
            $user->cedula = $request->input('cedula');
            $user->password = Hash::make($request->input('password'));
            $user->role_id = $request->input('role_id');
            $user->sede_id = $request->input('sede_id');
            $user->area_id = $request->input('area_id');
            $user->save();
    
            return response()->json(['ok'=>true,'msg'=>'El usuario fue actualizada correctamente'],200);
        }catch(Exception $e){
            return response()->json(['ok'=>false,'msg'=>'Error en los campos', 'err'=>$e],500);
        }
    }
}
