<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SedeModel extends Model
{
    use HasFactory;

    protected $table = "sedes";

    protected $fillable = [
        'nombre',
        'direccion',
        'centro_costo',
    ];

    public function user(){
        return $this->hasMany('App\Models\User');
    }

    public function orden (){
        return $this->hasMany('App\Models\OrdenModel');
    }

}
