<?php

namespace App\Http\Controllers;

use App\Models\RolesModel;
use Illuminate\Http\Request;

class RolesController extends Controller
{
    public function roles (){
        $roles = RolesModel::all();
        return response()->json(['ok'=>true,'roles'=>$roles],200);
    }
}
