<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VehiculoModel extends Model
{
    use HasFactory;

    
    protected $table = "vehiculos";

    protected $fillable = [
        'nombre',
        'marca_id'
    ];


    public function vehiculo(){
        return $this->belongsTo('App\Models\VehiculoModel');
    }
}
