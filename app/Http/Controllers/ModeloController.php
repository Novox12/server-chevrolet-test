<?php

namespace App\Http\Controllers;

use App\Models\ModeloModel;
use Illuminate\Http\Request;

class ModeloController extends Controller
{
    
    public function modelo()
    {
        $modelo = ModeloModel::all();
        return response()->json(['ok'=>true,'modelos'=>$modelo],200);
    }

    public function modelonew(Request $request)
    {
        $validateData = $request->validate([
            'modelo'=>'required|string|max:255'
        ]);
    
        ModeloModel::create([
            'modelo'=>$validateData['modelo']
        ]);
        
        return response()->json(['ok'=>true,'msg'=>'El modelo fue registrado correctamente'],201);
    }
    
    public function  modeloupdate($id)
    {
        # code...
    }
}
