<?php

namespace App\Http\Controllers;

use App\Models\VehiculoModel;
use Exception;
use Illuminate\Http\Request;

class VehiculosController extends Controller
{

    public function vehiculosall()
    {
        $vehiculo = VehiculoModel::join('marcas', 'marcas.id','=', 'vehiculos.marca_id')->select('vehiculos.id', 'vehiculos.nombre', 'vehiculos.created_at', 'vehiculos.marca_id', 'marcas.marca')->get();
        return response()->json(['ok'=>true,'vehiculos'=>$vehiculo],200);
    }

    public function vehiculos($id)
    {
        $vehiculo = VehiculoModel::select('*')->where('marca_id', $id)->get();
        return response()->json(['ok'=>true,'vehiculos'=>$vehiculo],200);
    }

    public function vehiculonew(Request $request)
    {
        $validateData = $request->validate([
            'nombre'=>'required|string|max:255',
            'marca_id'=>'required|integer'
        ]);
    
        VehiculoModel::create([
            'nombre'=>$validateData['nombre'],
            'marca_id'=>$validateData['marca_id']
        ]);
        
        return response()->json(['ok'=>true,'msg'=>'El vehículo fue registrado correctamente'],201);
    }

    public function  vehiculoupdate(Request $request , $id)
    {
        try{
            $vehiculo = VehiculoModel::find($id);
            $vehiculo->nombre = $request->input('nombre');
            $vehiculo->marca_id = $request->input('marca_id');
            $vehiculo->save();
    
            return response()->json(['ok'=>true,'msg'=>'La vehiculo fue actualizada correctamente'],200);
        }catch(Exception $e){
            return response()->json(['ok'=>false,'msg'=>'Error en los campos', 'err'=>$e],500);
        }
    }  
}
