<?php

namespace App\Http\Controllers;

use App\Models\OrdenModel;
use Illuminate\Http\Request;

class OrdenController extends Controller
{
    public function ordennew(Request $request)
    {
    
        $validateData = $request->validate([
            'documento'=>'required|string|max:255',
            'nombre'=>'required|string|max:255',
            'apellido'=>'required|string|max:255',
            'cedula'=>'required|string|max:255',
            'email'=>'required|string|max:255',
            'placa'=>'required|string|max:255',
            'vehiculo'=>'required|string|max:255',
            'marca'=>'required|string|max:255',
            'modelo'=>'required|string|max:255',
            'observaciones'=>'required|string|max:255',
            'fecha_inicio'=>'required',
            'fecha_fin'=>'required',
            'sede_id'=>'required|integer',
            'user_id'=>'required|integer'
        ]);

        
        $ordens1 = OrdenModel::select('*')->whereBetween('fecha_inicio', [$request->fecha_inicio, $request->fecha_fin])->where('sede_id', $request->sede_id)->get();

        $ordens2 = OrdenModel::select('*')->whereBetween('fecha_fin', [$request->fecha_inicio, $request->fecha_fin])->where('sede_id', $request->sede_id)->get();


        if(count($ordens1) == 0 && count($ordens2) == 0){

            OrdenModel::create([
                'documento'=>$validateData['documento'],
                'nombre'=>$validateData['nombre'],
                'apellido'=>$validateData['apellido'],
                'cedula'=>$validateData['cedula'],
                'email'=>$validateData['email'],
                'placa'=>$validateData['placa'],
                'vehiculo'=>$validateData['vehiculo'],
                'marca'=>$validateData['marca'],
                'modelo'=>$validateData['modelo'],
                'observaciones'=>$validateData['observaciones'],
                'fecha_inicio'=>$validateData['fecha_inicio'],
                'fecha_fin'=>$validateData['fecha_fin'],
                'sede_id'=>$validateData['sede_id'],
                'user_id'=>$validateData['user_id']
            ]);

            return response()->json(['ok'=>true,'msg'=>'La cita fue registra correctamente'],201);
            
        }else{
            return response()->json(['ok'=>false, 'msg'=>'La cita no fue creada, la cita coincide con otra orden de trabajo'],200);
        }

    }

    public function ordensede($id)
    {
        $ordens = OrdenModel::select('*')->where('sede_id', $id)->get();
        return response()->json(['ok'=>true,'ordenes'=>$ordens],200);
    }

}
