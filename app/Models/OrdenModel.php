<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrdenModel extends Model
{
    use HasFactory;

    
    protected $table = "ordens";

    

    protected $fillable = [
        'documento',
        'nombre',
        'apellido',
        'cedula',
        'email',
        'placa',
        'vehiculo',
        'marca',
        'modelo',
        'observaciones',
        'fecha_inicio',
        'fecha_fin',
        'sede_id',
        'user_id'
    ];

    public function sede(){
        return $this->belongsTo('App\Models\SedeModel');
    }

    public function user(){
        return $this->belongsTo('App\Models\User');
    }
}
