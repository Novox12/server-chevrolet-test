<?php

namespace App\Http\Controllers;

use App\Models\areasModel;
use Exception;
use Illuminate\Http\Request;

class AreasController extends Controller
{
    public function areas()
    {
        $areas = areasModel::all();
        return response()->json(['ok'=>true,'areas'=>$areas],200);
    }

    public function areasnew(Request $request)
    {
        $validateData = $request->validate([
            'nombre'=>'required|string|max:255'
        ]);
    
        areasModel::create([
            'nombre'=>$validateData['nombre']
        ]);
        
        return response()->json(['ok'=>true,'msg'=>'La area fue creada correctamente'],201);
    }
    
    public function  areasupdate(Request $request , $id)
    {
        try{
            $area = areasModel::find($id);
            $area->nombre = $request->input('nombre');
            $area->save();
    
            return response()->json(['ok'=>true,'msg'=>'La area fue actualizada correctamente'],200);
        }catch(Exception $e){
            return response()->json(['ok'=>false,'msg'=>'Error en los campos', 'err'=>$e],500);
        }
    }
}
